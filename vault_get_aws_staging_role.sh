#!/bin/bash
# run . script_name.sh to set env variables

export VAULT_TOKEN=$(vault write --field=token auth/approle/login role_id="" secret_id="")
vault read -format=json aws/creds/staging-user > tmp.json

ACCESS_KEY_ID=$(jq '.data.access_key' tmp.json)
export AWS_ACCESS_KEY_ID=$ACCESS_KEY_ID

SECRET_ACCESS_KEY=$(jq '.data.secret_key' tmp.json)
export AWS_SECRET_ACCESS_KEY=$SECRET_ACCESS_KEY

TOKEN=$(jq '.data.security_token' tmp.json)
export AWS_SESSION_TOKEN=$TOKEN

rm -f tmp.json
